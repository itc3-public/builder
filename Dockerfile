FROM docker:dind
ENV KUBERNETES_VERSION 1.5.2
ENV HELM_VERSION 2.3.1
ENV GOPATH="/gopath"
ENV GOBIN="/gopath/bin"
ARG VCS_REF
ARG BUILD_DATE
ENV BOOTSTRAP=true

RUN apk update \
    && apk add unzip curl tar go gcc libgcc libc-dev linux-headers libc-dev ca-certificates bash \
    python make bash vim jq \
    openssl openssh-client iputils drill \
    gcc libffi-dev python-dev musl-dev openssl-dev py-virtualenv \
    git coreutils less groff bash-completion && \
    mkdir /etc/bash_completion.d/

RUN echo "net.ipv6.conf.all.disable_ipv6=0" > /etc/sysctl.d/00-ipv6.conf

USER root

# Install kubectl
RUN curl --fail -sSL -O https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl \
    && mv kubectl /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && kubectl completion bash > /etc/bash_completion.d/kubectl.sh

# Install helm
RUN curl --fail -sSL -O http://storage.googleapis.com/kubernetes-helm/helm-v${HELM_VERSION}-linux-amd64.tar.gz \
    && tar -zxf helm-v${HELM_VERSION}-linux-amd64.tar.gz \
    && mv linux-amd64/helm /usr/local/bin/helm \
    && rm -rf linux-amd64 \
    && chmod +x /usr/local/bin/helm \
    && helm completion > /etc/bash_completion.d/helm.sh

# Install aws cli bundle
RUN curl --fail -sSL -O https://s3.amazonaws.com/aws-cli/awscli-bundle.zip \
    && unzip awscli-bundle.zip \
    && ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws \
    && rm awscli-bundle.zip \
    && rm -rf awscli-bundle \
    && ln -s /usr/local/aws/bin/aws_bash_completer /etc/bash_completion.d/aws.sh \
    && ln -s /usr/local/aws/bin/aws_completer /usr/local/bin/

# Install Google Cloud SDK
ENV GCLOUD_SDK_VERSION=147.0.0
RUN curl --fail -sSL -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    tar -zvxf google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    mv google-cloud-sdk /usr/local/ && \
    /usr/local/google-cloud-sdk/install.sh --quiet --rc-path /etc/bash_completion.d/gcloud.sh && \
    rm -rf google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    rm -rf /root/.config/ && \
    ln -s /usr/local/google-cloud-sdk/bin/gcloud /usr/local/bin/ && \
    ln -s /usr/local/google-cloud-sdk/bin/gsutil /usr/local/bin/ && \
    ln -s /usr/local/google-cloud-sdk/bin/bq /usr/local/bin/

ADD rootfs/ /

# ENTRYPOINT ["/bin/bash"]
CMD bash

